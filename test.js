//first NodeJS application 

//Link the application to the express framework
const express = require('express') 

//Create an instance of an express restful service. 
const app = express() 

// Define a port number for the service to listen on. 
const port = 3000 

/*Use the method get to create a hook on the server for get requests to the / (aka root) 
directory resource (first parameter of the get method). Also pass to get the method to be called 
when the resource is accessed (req,res) => {}. Inside the function we access the res object to 
.send the the text ‘hello virtual machine’. */

app.get('/test', (req, res) => { 
    res.send('Hello  Machine!') 
  }) 

  
app.get('/', (req, res) => { 
  res.send('Hello  Machine!') 
}) 

/*use the listen method to bind to the port, specified as the first argument (3000). 
For the second argument we pass in a method that calls console.log() to print some status to 
output of the terminal. */

app.listen(port, () => { 
  console.log(`Express Application  listening at port 3000`) 
}) 
